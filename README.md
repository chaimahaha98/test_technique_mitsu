1. Pour la création du projet symfony : 
* on tape la commande : symfony new testrec01 --webapp ( pour avoir une architecture compléte d'une application full stack Symfony) // la projet sera créé avec la derniere version de symfony qui est 6.3.3 ( par défaut)
** on fait aussi la création de la base des données avec "php bin/console doctrine:database:create" aprés la configuration de fichier .env et aprés avoir mis à jour le composer et installer npm / yarn (npm dans mon travail)


2. Pour pointer le domaine www.testrec01.local:5458 vers le dossier du projet créé afin de visualiser le
projet Symfony directement en tapant http://www.testrec01.local:5458 :
je devrais accéder à hosts localisé dans le dossier c:/windows/system23/drivers/etc en tant qu'admin et ajouter à la fin la ligne 127.0.0.1 www.testrec01.local et en ce qui concerne le port 5458, on lance le serveur symfony en indiquant le num de port 5458 avec la commande symfony serve --port=5458


3.  on installe tout d'abord le bundle security avec la commande : "composer require symfony/security-bundle"
puis on tape la commande "php bin/console make:user" en donnant par la suite les details de la classe user; le nom de la classe "User",  .. 
ensuite on fait la mise à jour de la base des donnees avec "php bin/console doctrine:schema:update -f"
finalement on créé le module d'authentification avec la commande "php bin/console make:auth"
**/ pour permettre l'utilisateur de se connecter, il nous exige avant tout de creer un module de registration avec la commande "symfony console make:registration-form" et on refait le mise à jour de la bd pour créer un compte


4. Pour gérer une facture on commence par créer l'entité facture en indiquant les attributs et leurs types avec la commande " php bin/console make:entity" puis fait le necessaire 
*** Puis on créer le crud de l'entité Facture avec php bin/console make:crud Facture on choisit le nom du controleur ...
// on mis  à jour le db à chaque fois qu'on travail sur l'entité ; on ajoute aussi la méthode adéquat pour le calcule de prix_ttc dans la classe Facture 
*** pour l'utilisation du framework bootsrap on doit l'installer avec npm  on tape alors "npm install bootstrap --save-dev"

5. Pour la pagination j'ai utilisé le bundle knp-paginator avec la commande "composer require knplabs/knp-paginator-bundle" pour l'installer , j'ai choisi ce bundle grâce à sa facilité d'utilisation


6. On peut tester l'affichage des factures paginées comme on peut tester l'affichagedes boutons de pagination

7. Guide : 
* 7.1. il faut tout d'abord installer les environnements necessaires pour une application symfony comme le composer, le php 8, Symfony Cli , un serveur locale ( xampp, wampp, laragon .. ) et aussi un éditeur de text ( sublimtext, PhpStorm..)
* 7.2. on commence par clonner le projet avec "git clone https://gitlab.com/chaimahaha98/test_technique_mitsu.git
* 7.3. on ouvre le projet avec l'editeur de text on tape "git checkout master" pour obtenir tout les fichiers poussées dans la branche choisit ( master)
* 7.4 on install le composer avec composer install / composer update
* 7.5. on install le npm 
* 7.6. on configure le fichier .env si vous voulez changer le nom de la base de données 
* 7.7. on crée la bd avec "php bin/console doctrine:database:create"
* 7.8. on mis à jour la db avec " php bin/console doctrine:schema:update -f" ou *** "php bin/console d:s:u -f"
* 7.9. on lance le serveur mtn avec " symfony serve 

 
